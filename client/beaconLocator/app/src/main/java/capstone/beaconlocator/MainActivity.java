package capstone.beaconlocator;

import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.bluetooth.BluetoothAdapter;

import com.github.nkzawa.socketio.client.Ack;
import com.wizturn.sdk.Proximity;
import com.wizturn.sdk.central.Central;
import com.wizturn.sdk.central.CentralManager;
import com.wizturn.sdk.peripheral.Peripheral;
import com.wizturn.sdk.peripheral.PeripheralScanListener;


import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.emitter.Emitter;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


import android.view.*;
import android.os.Handler;
import android.app.Activity;


public class MainActivity extends ActionBarActivity {
    private CentralManager centralManager;
    private final int REQUEST_ENABLE_BT = 1000;
    private Socket socket=null;
    private GpsInfo gps = null;
    private Activity main=this;
    private View register=null;
    private View track=null;

    public void registerClick(View view){

        register.setEnabled(false);
        track.setEnabled(false);
        try {
            socket = IO.socket("http://jmm.kr:8080");
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    CentralManager.getInstance().init(getApplicationContext());
                    CentralManager.getInstance().setPeripheralScanListener(new PeripheralScanListener() {
                        @Override
                        public void onPeripheralScan(Central central, final Peripheral peripheral) {
                            Log.i("BD NAME",peripheral.getBDName());
                            String BDName = peripheral.getBDName();
                            if(BDName.equals("pebBLE")){
                                Proximity p=peripheral.getProximity();

                                Log.i("TEST",p.toString());
                                Log.i("TEST", peripheral.getBDAddress());

                                if(p==Proximity.Immediate){
                                    try {
                                        CentralManager.getInstance().stopScanning();
                                        final String uuid = peripheral.getBDAddress();
                                        final double lat = gps.getLatitude();
                                        final double lng = gps.getLongitude();
                                        JSONObject json = new JSONObject();
                                        json.put("lat", lat);
                                        json.put("lng", lng);
                                        json.put("uuid", uuid);
                                        Log.i("TEST",json.toString());
                                        socket.emit("beaconRegister", json);
                                        socket.disconnect();
                                        main.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                View register = findViewById(R.id.register);
                                                register.setEnabled(true);
                                                track.setEnabled(true);
                                            }
                                        });

                                    }catch(JSONException ex){

                                    }
                                }
                            }


                        }
                    });

                    // TODO do something with the scanned peripheral(beacon)
                    // Log.i("ExampleActivity", "peripheral : " + peripheral);
                    if(!CentralManager.getInstance().isBluetoothEnabled()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                    }else{
                        CentralManager.getInstance().startScanning();
                    }

                }
            });

        }catch(URISyntaxException ex){

        }

        socket.connect();


    }

    public void trackClick(View view){

        //track.setEnabled(false);

        if(register.isEnabled()==false){
            CentralManager.getInstance().stopScanning();
            register.setEnabled(true);
            track.setEnabled(true);
            socket.disconnect();

        }else {

            register.setEnabled(false);
            try {
                socket = IO.socket("http://jmm.kr:8080");
                socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        CentralManager.getInstance().init(getApplicationContext());
                        CentralManager.getInstance().setPeripheralScanListener(new PeripheralScanListener() {
                            @Override
                            public void onPeripheralScan(Central central, final Peripheral peripheral) {
                                Log.i("BD NAME", peripheral.getBDName());
                                String BDName = peripheral.getBDName();
                                if (BDName.equals("pebBLE")) {
                                    Proximity p = peripheral.getProximity();

                                    Log.i("TEST", p.toString());
                                    Log.i("TEST", peripheral.getBDName());

                                    final String uuid = peripheral.getProximityUUID();
                                    final Integer rssi = peripheral.getRssi();
                                    final Integer measuredPower = peripheral.getMeasuredPower();
                                    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    f.setTimeZone(TimeZone.getTimeZone("UTC"));
                                    final String date = f.format(new Date());


                                    try {
                                        //CentralManager.getInstance().stopScanning();
                                        JSONObject json = new JSONObject();
                                        json.put("uuid", uuid);
                                        json.put("rssi", rssi);
                                        json.put("power", measuredPower);
                                        json.put("uid", 123);
                                        json.put("time", date);
                                        Log.i("TEST", json.toString());
                                        socket.emit("userLocation", json);
                                        //socket.disconnect();


                                    } catch (JSONException ex) {

                                    }

                                }


                            }
                        });

                        // TODO do something with the scanned peripheral(beacon)
                        // Log.i("ExampleActivity", "peripheral : " + peripheral);
                        if (!CentralManager.getInstance().isBluetoothEnabled()) {
                            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                        } else {
                            CentralManager.getInstance().startScanning();
                        }

                    }
                });

            } catch (URISyntaxException ex) {

            }

            socket.connect();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gps=new GpsInfo(this);

        register=findViewById(R.id.register);
        track=findViewById(R.id.track);




    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                // TODO Okay. Now bluetooth is on. Let's scan.
                CentralManager.getInstance().startScanning();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
