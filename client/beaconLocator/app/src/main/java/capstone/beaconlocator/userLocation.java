package capstone.beaconlocator;

import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.bluetooth.BluetoothAdapter;

import com.github.nkzawa.socketio.client.Ack;
import com.wizturn.sdk.Proximity;
import com.wizturn.sdk.central.Central;
import com.wizturn.sdk.central.CentralManager;
import com.wizturn.sdk.peripheral.Peripheral;
import com.wizturn.sdk.peripheral.PeripheralScanListener;


import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.emitter.Emitter;

import android.util.Log;

import org.json.JSONException;import org.json.JSONObject;

import java.net.URISyntaxException;


import android.view.*;
import android.os.Handler;
import android.app.Activity;

import java.util.*;
import java.text.*;

public class userLocation extends ActionBarActivity{
    private CentralManager centralManager;
    private final int REQUEST_ENABLE_BT = 1000;
    private boolean track=false;


    public void onClick(View v) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final GpsInfo gps = new GpsInfo(this);
        View button=findViewById(R.id.track);
        final Activity main=this;

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (track==false) {
                    View button = findViewById(R.id.track);

                    CentralManager.getInstance().init(getApplicationContext());
                    CentralManager.getInstance().setPeripheralScanListener(new PeripheralScanListener() {
                        @Override
                        public void onPeripheralScan(Central central, final Peripheral peripheral) {
                            Log.i("BD NAME", peripheral.getBDName());
                            String BDName = peripheral.getBDName();
                            if (BDName.equals("pebBLE")) {
                                Integer lat = peripheral.getMeasuredPower();
                                Proximity p = peripheral.getProximity();


                                final String uuid = peripheral.getBDAddress();
                                final Integer rssi = peripheral.getRssi();
                                final Integer measuredPower = peripheral.getMeasuredPower();
                                SimpleDateFormat f = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
                                f.setTimeZone(TimeZone.getTimeZone("UTC"));
                                final String date = f.format(new Date());
                                if (p == Proximity.Immediate || p == Proximity.Near) {
                                    final Socket socket;
                                    try {
                                        socket = IO.socket("http://jmm.kr:8080");
                                        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                                            @Override
                                            public void call(Object... args) {
                                                try {
                                                    JSONObject json = new JSONObject();

                                                    json.put("uuid", uuid);
                                                    json.put("rssi", rssi);
                                                    json.put("power", measuredPower);
                                                    json.put("uid", 123);
                                                    json.put("date", date);
                                                    socket.emit("userLocation", json, new Ack() {
                                                        @Override
                                                        public void call(Object... args) {
                                                            JSONObject obj=(JSONObject)args[0];
                                                            try {

                                                                if(obj.get("result")=="success"){

                                                                }
                                                            }catch (JSONException ex){

                                                            }


                                                        }
                                                    });
                                                    socket.disconnect();


                                                } catch (JSONException ex) {

                                                }

                                            }

                                        });
                                        socket.connect();
                                    } catch (URISyntaxException ex) {

                                    }

                                }
                            }


                        }

                    });

                    // TODO do something with the scanned peripheral(beacon)
                    // Log.i("ExampleActivity", "peripheral : " + peripheral);
                    if (!CentralManager.getInstance().isBluetoothEnabled()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                    } else {
                        CentralManager.getInstance().startScanning();
                    }

                }else{
                    CentralManager.getInstance().stopScanning();
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                // TODO Okay. Now bluetooth is on. Let's scan.
                CentralManager.getInstance().startScanning();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
