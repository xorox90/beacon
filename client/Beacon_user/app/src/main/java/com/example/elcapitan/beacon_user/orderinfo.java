package com.example.elcapitan.beacon_user;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;


public class orderinfo extends ActionBarActivity {
    Intent Service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderinfo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_orderinfo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startBeaconTrack(){
        Log.i("StartBeaconTrack", "Start");
        startService(Service);
    }

    public void stopBeaconTrack(){
        Log.i("StopBeaconTrack", "Stop");
        stopService(Service);
    }

    public void orderClick(View view){
        Service = new Intent(this,Beacon_track.class);
        Log.i("start", "track");
        startBeaconTrack();
        try {
            final TextView v_pid = (TextView)findViewById(R.id.pid1);
            final EditText v_quantity = (EditText)findViewById(R.id.quantity1);
            final Socket socket = IO.socket("http://jmm.kr:8080");
            final String s_quantity = v_quantity.getText().toString();
            final int pid = 1;
            final int quantity = Integer.parseInt(s_quantity);
            final String order;
            JSONObject json = new JSONObject();
            JSONArray jarray = new JSONArray();
            try{
                JSONObject jObject = new JSONObject();
                try{
                    jObject.put("quantity",quantity);
                    jObject.put("pid",pid);
                    jarray.put(jObject);
                }catch(JSONException ex){
                }
                json.put("sid",1);
                json.put("items",jarray);
                order=json.toString();
                Log.i("order", order);
                socket.emit("orderInformation", json, new Ack() {
                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        try {
                            if (obj.get("result").equals("success")) {
                                Log.i("OrderSuccess", order);
                            }
                            if (obj.get("result").equals("fail")) {
                                Log.i("OrderFailed", order);
                            }
                        } catch (JSONException ex) {
                        }
                    }
                });
            }catch(JSONException ex){
            }
        }catch(URISyntaxException ex){

        }
    }
}
