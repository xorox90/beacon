package com.example.elcapitan.beacon_user;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.IO;

import java.net.URISyntaxException;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;



public class register extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    public void OrderMain(){
        Intent intent=new Intent(this, OrderMain.class);
        startActivity(intent);
    }

    public void registerClick(View view){
        final AutoCompleteTextView rEmailView = (AutoCompleteTextView)findViewById(R.id.register_email);
        final EditText rPasswordView = (EditText)findViewById(R.id.register_password);
        final String r_email = rEmailView.getText().toString();
        final String r_password = rPasswordView.getText().toString();
        Toast toast = Toast.makeText(this, r_email, Toast.LENGTH_SHORT);
        toast.show();
        try {
            final Socket socket = IO.socket("http://jmm.kr:8080");
            try{
                JSONObject json = new JSONObject();
                json.put("cuid",r_email);
                json.put("password",r_password);
                socket.emit("customerSignUp", json, new Ack() {
                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        try {
                            if (obj.get("result").equals("success")) {
                                Log.i("RegisterSuccess",r_email);
                            }
                            if (obj.get("result").equals("dup")) {
                                Log.i("RegisterFailed",r_email);
                            }
                        } catch (JSONException ex) {

                        }
                    }
                });
            }catch(JSONException ex){

            }
        }catch(URISyntaxException ex){

        }
        OrderMain();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
