package com.example.elcapitan.beacon_user;

import android.app.NotificationManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.bluetooth.BluetoothAdapter;
import android.widget.Toast;
import android.os.StrictMode;
import android.util.Log;

import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.emitter.Emitter;

import com.wizturn.sdk.Proximity;
import com.wizturn.sdk.central.Central;
import com.wizturn.sdk.central.CentralManager;
import com.wizturn.sdk.peripheral.Peripheral;
import com.wizturn.sdk.peripheral.PeripheralScanListener;

import org.json.JSONException;
import org.json.JSONObject;
import java.net.URISyntaxException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

/**
 * Created by elcapitan on 2015. 5. 22..
 */
public class Beacon_track extends Service{
    private Socket socket = null;
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        CentralManager.getInstance().init(getApplicationContext());
        CentralManager.getInstance().setPeripheralScanListener(new PeripheralScanListener() {
            @Override
            public void onPeripheralScan(Central central, final Peripheral peripheral) {
                Log.i("BD Name", peripheral.getBDName());
                String BDName = peripheral.getBDName();
                if (BDName.equals("pebBLE")) {
                    final String uuid = peripheral.getBDAddress();
                    Log.i("uuid", uuid);
                    Date from = new Date();
                    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    f.setTimeZone(TimeZone.getTimeZone("UTC"));
                    final String time = f.format(from);
                    try {
                        final Socket socket = IO.socket("http://jmm.kr:8080");
                        //socket.connect();
                        try {
                            JSONObject json = new JSONObject();
                            json.put("uuid", uuid);
                            json.put("time", time);
                            socket.emit("customerLocation", json, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    JSONObject obj = (JSONObject) args[0];
                                    try {
                                        if (obj.get("result").equals("success")) {
                                            Log.i("Send success", uuid);
                                        }
                                        if (obj.get("result").equals("finish")) {
                                            Log.i("Send finish", uuid);
                                        }
                                        if (obj.get("result").equals("fail")) {
                                            Log.i("Send failed", uuid);
                                        }
                                    } catch (JSONException ex) {

                                    }
                                }
                            });
                        } catch (JSONException ex) {

                        }
                    } catch (URISyntaxException ex) {

                    }
                }

                if (BDName.equals("RECO")) {
                    final String uuid = peripheral.getBDAddress();
                    Log.i("uuid", uuid);
                    Date from = new Date();
                    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    f.setTimeZone(TimeZone.getTimeZone("UTC"));
                    final String time = f.format(from);
                    try {
                        final Socket socket = IO.socket("http://jmm.kr:8080");
                        //socket.connect();
                        try {
                            JSONObject json = new JSONObject();
                            json.put("uuid", uuid);
                            json.put("time", time);
                            socket.emit("customerLocation", json, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    JSONObject obj = (JSONObject) args[0];
                                    try {
                                        if (obj.get("result").equals("success")) {
                                            Log.i("Send success", uuid);
                                        }
                                        if (obj.get("result").equals("finish")) {
                                            Log.i("Send finish", uuid);
                                        }
                                        if (obj.get("result").equals("fail")) {
                                            Log.i("Send failed", uuid);
                                        }
                                    } catch (JSONException ex) {

                                    }
                                }
                            });
                        } catch (JSONException ex) {

                        }
                    } catch (URISyntaxException ex) {

                    }
                }
            }
        });
        if (!CentralManager.getInstance().isBluetoothEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            //startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            CentralManager.getInstance().startScanning();
        }
        return START_NOT_STICKY;
//      return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i("Beacon Track", "Stop");
        CentralManager.getInstance().stopScanning();
        super.onDestroy();
    }
}

