var util = require('util');
var fs = require('fs');


var setting = require('./setting');
var database = require('./database');

var mysql=database.mysql;
var connection=database.connection;

//Shop server part
var path = require('path');
var express = require('express');
var app = express();
var swig = require('swig');
var serveStatic = require('serve-static');
var mysql = require('mysql');
var http = require('http');



// DB를 연결합니다.
var connection = mysql.createConnection({
	host: '127.0.0.1',
	port: 3306,
	user: 'capstone',
	password: 'capstone2015',
	database: 'capstone'	
});
connection.connect(function(err) {

	if(err) {
		console.error('mysql connection error');
		console.error(err);
	}

});

// view engine setup
app.engine('html', swig.renderFile);

app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views'));


app.use(serveStatic(__dirname + '/public'));


app.use('/shop/:shop_session', function(req, res, next) {
  //var fn = function (result) { console.log('RESULT:', result); };
  var shop;
  var order;

  var shopId;
  
  connection.query('SELECT * FROM Shop WHERE session = ?',[req.params.shop_session], function(err, rows, fields) {
    if (err) throw err;
    shop = rows;
    shopId = rows[0].sid;
    //console.log('The Shop Information is: ', shop);

    connection.query('SELECT * FROM OrderInformation WHERE sid = ?',[shopId], function(err, rows, fields) {
      if (err) throw err;
      order = rows;
      //console.log('The OrderInformation is: ', order);

      res.render('shop', {shopInfo : shop, OrderInfo : order});
    });
  });  
});

app.use('/shop', function(req, res, next) {
	res.render('index');
});

var server=http.createServer(app).listen(setting.port);




//반드시 setting 파일을 만들고 
//{
//	"serverPort":포트번호
//} 형식으로 작성해줘야함.
var io = require('socket.io')(server);
var mysql = require('mysql');
var async = require('async');
var logging = true;
var beaconLog = 0;


var beacons = {};
var shops= {};
var customers ={};


var readline = require('readline');

var rl = readline.createInterface({
				  input: process.stdin,
				    output: process.stdout
					});

rl.on('line', function(line){
		var key=line[0];
		if(line.length==1) {
				switch(key) {
					case 'c':
						showCustomerList();
						break;
					case 's':
						showShopList();
						break;
					case 'l':
						logging = !logging;
						console.log('logging = ' + logging);
						break;
					case 'q':
						process.exit(0);
						break;
					case 'z':
						
				}
		}else{
	
				try{
					eval(line);
				}catch(exception){
					console.log(exception);
					
				}
		
		}
	});



var detect = {};

function beaconRefresh(){
	connection.query('SELECT * FROM BeaconInformation', function(error,rows,fields){
	
		beacons={};
		for(i=0; i<rows.length; i++){
			sid=rows[i].sid;
			uuid=rows[i].uuid;
			
			if(!beacons[sid]){
				beacons[sid]={};
			}
			
			beacons[sid][uuid]=rows[i];
		}
	});

}


function notify(cid,oid,sid,result,cb){	

		var customer=customers[cid];
		
		var ret = null;
		console.log("notify",cid,oid,sid,result);

		connection.query('SELECT * FROM OrderInformation o, Customer c, ItemList i, Product p WHERE o.cid = c.cid AND o.oid = i.oid AND i.pid = p.pid AND o.oid = ?',oid, function(err, rows, fields) {
			if (err){
				console.error(err.stack);
				
				if(cb)
					cb(new Error('query error'), null);
				
				return;
			}

			for(var i = 0; i < rows.length; i++) {
				if(!ret)
				ret={
					oid: rows[i].oid,
					cuid: rows[i].cuid,
					phone: rows[i].phone,
					orderTime: rows[i].orderTime,
					receiveTime: rows[i].receiveTime,
					state: rows[i].state,
					items:[]
				}

				ret.items.push({pname: rows[i].pname,quantity: rows[i].quantity});
			}

			console.log(ret);
			if(cb)
				cb(null,null);

			ret.result=result;
			if(shops[sid]){
				var socketId=shops[sid].socketId;
				var shopSocket=io.sockets.connected[socketId];
				shopSocket.emit('shopNotify',ret);
			}

				
		});




}

function showHelloMessage() {
	var msg = '************************************************************\n'
			+ 'The server is now running at port number( ' + setting.port + ')\n'
			+ 'Copyright (c) 2015 by SKKU Team ASCII\n'
			+ 'You can show the log message pressing some keys\n'
			+ ' \'c\': showCustomerList\n'
			+ ' \'s\': showShopList\n'
			+ ' \'b\': showBeaconList\n'
			+ ' \'l\': toggle showing log message state\n'
			+ ' \'q\': quit\n'
			+ '************************************************************';
	console.log(msg);
}

function showCustomerList() {
	console.log('******************* current customers *******************');
	for(var cus in customers) {
		console.log(util.format('%j', customers[cus]));
	}
	console.log('*********************************************************');
}

function showShopList() {
	console.log('********************* current shops *********************');
	for(var shop in shops) {
		console.log(util.format('%j', shops[shop]));
	}
	console.log('*********************************************************');
}

function showBeaconLog() {
	var uuid = '';
	console.log('******************** current beacons *******************');
	for (uuid in detect) {
		if(detect[uuid]) {
			console.log(uuid);
			detect[uuid] = false;
		}
	}
	console.log('*********************************************************');
}


showHelloMessage();
setInterval(beaconRefresh,1000);

io.sockets.on('connection', function(socket) {
	//socket.emit('example message 1', { hello: 'world 1' });
	//socket.on('example message 2', function(data) {
	//	console.log(data);	
	//});
	
	// 소비자가 회원가입을 요청한 이벤트
	
	

	socket.on('customerSignUp', function(data,fn) {
		// 계정 중복을 확인하기 위한 쿼리문
		var query = mysql.format('SELECT * FROM Customer WHERE cuid=?', data.cuid);
		if(logging) {
			console.log('[CustomerSignUp message]');
			console.log(data);
			console.log(query);
		}
		connection.query(query, function(err, rows, fields) {
			if(err) {
				console.error(err.stack);
				fn({ 'result': 'query error' });
			}
			else {
				// 중복되는 계정이 없으면
				if(rows[0] === undefined) {
					// 계정 삽입을 위한 쿼리문
					query = mysql.format('INSERT INTO Customer(cuid, password, cname, phone) VALUES(?, ?, ?, ?)'
						, [data.cuid, data.password, data.cname, data.phone]);
					if(logging) {
						console.log(query);
					}
					connection.query(query, function(err, rows, fields) {
						if(err) {
							console.error(err.stack);
							fn({ 'result': 'query error' });
						}
						else {
							fn({ 'result': 'success' });
						}
					});
				}
				// 중복되는 계정이 있는 경우
				else {
					fn({ 'result': 'dup' });
				}
			}
		});
	});

	// 소비자가 로그인 하려는 이벤트
	socket.on('customerSignIn', function(data,fn) {
		// 소비자가 입력한 계정과 암호에 일치하는 계정이 있는 지
		var query = mysql.format('SELECT * FROM Customer WHERE cuid=? AND password=?', 
				[data.cuid, data.password]);
		if(logging) {
			console.log('[CustomerSignIn message]');
			console.log(data);
			console.log(query);
		}
		connection.query(query, function(err, rows, fields) {
			if(err) {
				console.error(err.stack);
				fn({ 'result': 'query error' });
			}
			else {
				// 일치하는 계정이 있는 경우
				if(rows[0] !== undefined) {
					// 소비자 리스트에 추가
					var cid=rows[0].cid;
					socket.cid=cid;
					customers[cid]=rows[0];
					customers[cid].orders={};
					

					fn({ 'result': 'success' });
				}
				// 일치하는 계정이 없는 경우
				else {
					fn({ 'result': 'fail' });
				}
			}
		});
	
	});

	socket.on('shopList', function(data, fn) {
		// 서비스 하는 모든 물품을 조회하기 위한 쿼리문
		var query = mysql.format('SELECT Shop.sid, sname, pid, pname, price FROM Shop, Product WHERE Shop.sid = Product.sid');
		if(logging) {
			console.log('[shopList message]');
			console.log(data);
			console.log(query);
		}
		connection.query(query, function(err, rows, fields) {
			if(err) {
				console.error(err.stack);
				fn({ 'result': 'fail' });
			}
			else {
				// 클라이언트에게 전송할 자료형태
				var ret = {
					result: 'success',
					response: []
				};

				var list = [];
				for(var i = 0; i < rows.length; i++) {
					// 가게 항목 생성
					if(list[rows[i].sid] == undefined) {
						list[rows[i].sid] = {
							'sid': rows[i].sid,
							'sname': rows[i].sname,
							'products': []
						};
					}
					// 물품 항목 생성
					list[rows[i].sid]['products'].push({
						'pid': rows[i].pid,
						'pname': rows[i].pname,
						'price': rows[i].price
					});
				}

				for(var i in list) {
					ret.response.push(list[i]);
				}

				if(logging) {
					console.log(ret);
					for(var i in ret.response) {
						console.log(ret.response[i].products);
					}
				}

				fn(ret);
			}
		});
					
	});

	// 소비자로부터 주문이 들어온 이벤트
	socket.on('orderInformation', function(data,fn) {
		// 로그를 출력하고 전송받은 주문정보를 DB에 저장합니다.
		// OrderInfo( oid, uid, sid, pid, otime, rtime, state)
		var cid=socket.cid;
		var customer=customers[cid];

		if(!customer){
			fn({'result':'login first'});
			return;
		}

		var query = mysql.format('INSERT INTO OrderInformation(cid, sid) VALUES(?, ?)'
					, [socket.cid, data.sid]);

		if(logging) {
			console.log('[OrderInfo message]');
			console.log(data);
			console.log(query);
		}
		connection.query(query, function(err, result) {
			if(err) {
				console.error(err.stack);
				fn({result:'fail'});
				return;

			}
				
				var oid=result.insertId;
				var sid=data.sid;

				var arr=[];
				for(var i in data.items){
					var item=data.items[i];
					arr.push([sid,item.pid]);
				}
				connection.query('SELECT COUNT(*) as num FROM Product WHERE (sid,pid) IN  ?', [[arr]], function(err,rows,fields){
					if(err){
						console.error(err.stack);
						fn({result:'fail'});
						return;
					}

					if(rows.length){
						if(rows[0].num!=arr.length){
						fn({result:'invalid order'});
						return;
						}

						
						var arr2=[];
						for(var i in data.items){
							var item=data.items[i];
							arr2.push([item.pid,item.quantity,oid]);
						}	

						connection.query('INSERT INTO ItemList (pid,quantity,oid) VALUES ?', [arr2], function(err,rows,fields){
							if(err){
								console.error(err.stack);
								fn({result:'fail'});
								return;
							}

							customers[cid].orders[oid]={sid:sid};
							notify(cid,oid,sid,'order',function(){});
							fn({result:'success'});
						});

					}

				});

		});

	});

	// shop id 에 대한 shop connection 을 기억한다.
	socket.on('shopRegister', function(data,fn){
		console.log("registered");
		var query = mysql.format('SELECT * FROM Shop WHERE session=?',data.session);
		console.log(query);
		var order;
		var shopInfo;
		connection.query(query,function(err,rows,fields){
			if(err){
				console.log(err.stack);
				fn({result:'fail'});
			}
			if(rows.length){
				var sid=rows[0].sid;
				socket.sid=sid;
				shops[sid]=rows[0];
				shops[sid].socketId=socket.id;
				shops[sid].state=0;

				fn({result:'success'});
			}else{
				fn({result:'fail'});
			}
		});

	});

	// 소비자로부터 위치정보가 들어온 이벤트
	socket.on('customerLocation', function(data,fn) {
		// 로그를 출력하고 전송받은 위치정보를 DB에 저장합니다.
		// UserLocation( uid, uuid, rssi, time)

		var cid=socket.cid;
		var customer=customers[cid];

		if(!customer){
			fn({result:'login first'});
			return;
		}

		var query = mysql.format("INSERT INTO CustomerLocation VALUES(?, ?,?)"
					, [cid, data.uuid, data.time]);

		if(logging) {
			console.log('[CustomerLocation message]');
			console.log(data);
			console.log(query);
		}

		connection.query(query, function(err, rows, fields) {
			if(err) {
				console.error(err.stack);
			}
		});
		
		detect[data.uuid] = true;


		for(var oid in customer.orders){

			var sid = customer.orders[oid].sid;

			beacon=beacons[sid][data.uuid];
			if(beacon==undefined){
				fn({result:'fail'});
				return;
			}else{
				console.log(beacon);
				if(beacon.state==1){
					notify(cid,oid,sid,'prepare',function(){});

					fn({result:'finish'});
					delete customer.orders[oid]
					console.log('*****************************');		
					return;

				}
			}
		}

	});

	socket.on('orderFinished', function(data, fn) {
		var query = mysql.format("SELECT * FROM OrderInformation WHERE oid=?"
					, [data.oid]);

		if(logging) {
			console.log('[orderFinished message]');
			console.log(data);
			console.log(query);
		}

		connection.query(query, function(err, rows, fields) {
			if(err) {
				console.error(err.stack);
				fn({result:'fail'});
			}
			else {
				if(rows.length == 0) {
					fn({result:'oid is not available'});
					return ;
				}
				
				query = mysql.format("UPDATE OrderInformation SET state=2, receiveTime=sysdate() WHERE oid=?"
						, [data.oid]);
				if(logging) 
					console.log(query);
				connection.query(query, function(err, rows, fields) {
					if(err) {
						console.error(err.stack);
						fn({result:'fail'});
					}
				});
				fn({result:'success'});
			}
		});
		
	});	

	socket.on('changeShopState', function(data, fn){

		console.log(data);

		if(!socket.sid){
			fn({result:'login first!'});
			return;
		}
		console.log(data.shopPrepareationTime);
		if(typeof data.shopPreparationTime != 'number'){
			fn({result:'invalid argument'});
			return;
		}

		var query = mysql.format('UPDATE Shop SET state= ? WHERE sid= ?',
				[data.shopPreparationTime,socket.sid]);

		if(logging) {
			console.log('[changeShopState message]');
			console.log(data);
			console.log(query);
		}

	    connection.query(query, function(err, rows, fields) {
			if(err) {
				console.error(err.stack);
				fn({result:'fail'});
			}
			else {
				connection.query(mysql.format('UPDATE BeaconInformation SET state=0 WHERE sid=? AND travelTime > ?', [socket.sid, data.shopPreparationTime]));
				query = mysql.format('UPDATE BeaconInformation SET state=1 WHERE sid=? AND travelTime <= ?',
						[socket.sid, data.shopPreparationTime]);
				if(logging) {
					console.log(query);
				}
				

				connection.query(query, function(err, rows, fields) {
					if(err) {
						console.error(err.stack);
						fn({result:'fail'});
					}
					else {
						socket.emit('acceptShopState', data);
						beaconRefresh();
						fn({ result: 'success' });
					}
				});
			
			}
	    });
   	});


	socket.on('getShopOrder', function(data, fn){

		if(!socket.sid){
			fn({result:'login first!'});
			return;
		}

		if(logging) {
			console.log('[getShopOrder message]');
			console.log(data);
		}
		
		var list = [];
		var ret = {};
		/* ret = {
			oid: 0,
			cname: "",
			phone: 0,
			orderTime: "",
			receiveTime: "",
			state: 0,
			items = [];
		};
		*/
		connection.query('SELECT * FROM OrderInformation o, Customer c, ItemList i, Product p WHERE o.cid = c.cid AND o.oid = i.oid AND i.pid = p.pid AND o.sid = ?',[socket.sid], function(err, rows, fields) {
			if (err){
				console.error(err.stack);
				fn({result:'fail'});
				return;
			}

			for(var i = 0; i < rows.length; i++) {
				if(ret[rows[i].oid]===undefined)
				ret[rows[i].oid]={
					oid: rows[i].oid,
					cuid: rows[i].cuid,
					phone: rows[i].phone,
					orderTime: rows[i].orderTime,
					receiveTime: rows[i].receiveTime,
					state: rows[i].state,
					items: []
				}

				ret[rows[i].oid].items.push({pname: rows[i].pname,quantity: rows[i].quantity});
			}
			for(var idx in ret) {
				list.push(ret[idx]);
			}
			console.log(list);
			fn(list);
				
		});
	});

	// 비콘 설치시 비콘의 위치정보가 들어온 이벤트
	socket.on('beaconRegister', function(data) {
		// 로그를 출력하고 전송받은 위치정보를 DB에 저장합니다.
		var query = mysql.format("INSERT INTO BeaconLocation values('?', ?, ?) ON DUPLICATE KEY UPDATE lng=?, lat=?"
					, [data.uuid, data.lng, data.lat, data.lng, data.lat]);
		if(logging) {
			console.log('[beaconRegister message]');
			console.log(data);
			console.log(query);
		}
		connection.query(query, function(err, rows, fields) {
			if(err) {
				console.error(err.stack);
			}
		});
	});
	
	socket.on('disconnect', function() {
		console.log('[disconnected message]');	
		if(socket.cid !== undefined) {
			console.log('customer cid(' + socket.cid + ') disconneted.');	
			delete customers[socket.cid];
		}
		if(socket.sid !== undefined) {
			console.log('shop sid(' + socket.sid + ') disconnected.');
			delete shops[socket.sid];
		}
	});
});
