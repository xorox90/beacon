var fs = require('fs');


var setting = require('../setting');
//반드시 setting 파일을 만들고 
//{
//	"serverPort":포트번호
//} 형식으로 작성해줘야함.

var io = require('socket.io-client');
var customer = io('http://localhost:'+setting.port,{multiplex:false});
var shop = io('http://localhost:'+setting.port,{multiplex:false});
//var customer;
//var shop;

var async = require('async');
var assert = require('assert');
var database = require('../database');
var mysql = database.mysql;
var connection = database.connection;

//aa 계정이 존재해야함
//shop과 product가 존재해야함

async.series([
			function customerConnect(cb){
				customer.on('connect', function(){
						cb(null,null);
				});

			},function shopConnect(cb){
				shop.on('connect', function(){
						cb(null,null);
					});
			}, function shopRegister(cb){
					console.log('shop register');
					shop.emit('shopRegister', {'session':'aaa'},function(data){
						assert.equal(data.result,'success');
						cb(null,null);	
					});

					shop.on('shopNotify', function(data){
						
						console.log(data);
						if(data.result=='order')
							console.log('shop order notify');
						else if(data.result=='prepare'){
							console.log('shop prepare notify');
							console.log('test end');
							process.exit(0);
						}
					});

		   	}, function signIn(cb){
				console.log('signin');
				customer.emit('customerSignIn',{cuid:'aa',password:'aa'},function(data){
					assert.equal(data.result,'success');
					cb(null,null);

				});
			}, function order(cb){
				console.log('order');
				customer.emit('orderInformation',{sid:1,items:[{pid:1,quantity:1}]},function(data){
					assert.equal(data.result,'success');
					cb(null,null);

				});
			   
			}, function customerLocation(cb){
				console.log('location');
				var stop=false;
				for(var i=0; i<100; i++){
					setTimeout(function(i){
							if(stop)
							return;

							console.log('location emit');
							customer.emit('customerLocation', {uuid:i,time:0},function(data){
								console.log(data);
								if(data.result=='finish'){
									stop=true;
									cb(null,null);

								}
								
									
							});
					},i*100, i);
				}

			}], function(err,result){
				if(err)
					console.log(err);

			});


