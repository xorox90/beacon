var fs = require('fs');
var setting = require('../setting');
var socket = require('socket.io-client')('http://localhost:'+setting.port);
var shopState = {
	'shopId' : Number(process.argv[2]) ,
	'shopPreparationTime' : Number(process.argv[3]),
};

if(shopState.shopId === undefined || shopState.shopPreparationTime === undefined) {
	console.log('undefined');
	process.exit(0);
}
	
socket.on('connect', function() {
	socket.emit('changeShopState', shopState, function(data) {
		console.log(data);		
		process.exit(0);
	});
});
