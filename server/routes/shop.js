var express = require('express');
var router = express.Router();
var mysql      = require('mysql');


var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'capstone',
  password : 'capstone2015',
  database : 'capstone'
});

var shop;
var order;


connection.connect();

/* GET shop page. */
router.get('/', function(req, res, next) {

  var shopSession = 'aaa';
  var shopId;
  connection.query('SELECT * FROM Shop WHERE session = ?',[shopSession], function(err, rows, fields) {
    if (err) throw err;
    shop = rows;
    shopId = rows[0].sid;
    console.log('The Shop Information is: ', shop);

    connection.query('SELECT * FROM OrderInformation WHERE sid = ?',[shopId], function(err, rows, fields) {
      if (err) throw err;
      order = rows;
      console.log('The OrderInformation is: ', order);

      res.render('shop', {shopInfo : shop, OrderInfo : order});
    });
  });
});


module.exports = router;
